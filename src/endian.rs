//! This module helps with the endian in different files.
extern crate byteorder;

use endian::byteorder::{ByteOrder, LittleEndian, BigEndian, NativeEndian};
use std::io::{Read, Write};
use std::io;
/// Enum which contains the Byteorder
#[derive(Copy, Clone)]
pub enum Endian {
    LITTLE,
    BIG
}

/// Returns the host endian
pub fn host_endian() -> Endian {
    let mut buf = [0; 4];
    LittleEndian::write_u32(&mut buf, 0x12345678);
    match NativeEndian::read_u32(&buf) {
        0x12345678 => Endian::LITTLE,
        _ => Endian::BIG
    }
}

/// Converts an u16 from one endian to the native one and vice versa
pub fn convert_u16(val: u16, e: Endian) -> u16 {
    let mut buf = [0; 2];
    match e {
        Endian::LITTLE => LittleEndian::write_u16(&mut buf, val),
        Endian::BIG => BigEndian::write_u16(&mut buf, val)
    };
    NativeEndian::read_u16(&buf)
}

/// Reads an u16 from one endian from a Read
pub fn read_u16(inp: &mut Read, e: Endian) -> io::Result<u16> {
    let mut buf = [0; 2];
    inp.read(&mut buf)?;
    Ok(convert_u16(NativeEndian::read_u16(&buf), e))
}

/// Writes an u16 as one endian for a write
pub fn write_u16(val: u16, out: &mut Write, e: Endian) -> io::Result<()> {
    let mut buf = [0; 2];
    NativeEndian::write_u16(&mut buf, convert_u16(val, e));
    out.write(&buf)?;
    Ok(())
}

/// Converts an u32 from one endian to the native one and vice versa
pub fn convert_u32(val: u32, e: Endian) -> u32 {
    let mut buf = [0; 4];
    match e {
        Endian::LITTLE => LittleEndian::write_u32(&mut buf, val),
        Endian::BIG => BigEndian::write_u32(&mut buf, val)
    };
    NativeEndian::read_u32(&buf)
}

/// Reads an u32 from one endian from a Read
pub fn read_u32(inp: &mut Read, e: Endian) -> io::Result<u32> {
    let mut buf = [0; 4];
    inp.read(&mut buf)?;
    Ok(convert_u32(NativeEndian::read_u32(&buf), e))
}

/// Writes an u32 as one endian for a write
pub fn write_u32(val: u32, out: &mut Write, e: Endian) -> io::Result<()> {
    let mut buf = [0; 4];
    NativeEndian::write_u32(&mut buf, convert_u32(val, e));
    out.write(&buf)?;
    Ok(())
}

/// Converts an u64 from one endian to the native one and vice versa
pub fn convert_u64(val: u64, e: Endian) -> u64 {
    let mut buf = [0; 8];
    match e {
        Endian::LITTLE => LittleEndian::write_u64(&mut buf, val),
        Endian::BIG => BigEndian::write_u64(&mut buf, val)
    };
    NativeEndian::read_u64(&buf)
}

/// Reads an u64 from one endian from a Read
pub fn read_u64(inp: &mut Read, e: Endian) -> io::Result<u64> {
    let mut buf = [0; 8];
    inp.read(&mut buf)?;
    Ok(convert_u64(NativeEndian::read_u64(&buf), e))
}

/// Writes an u64 as one endian for a write
pub fn write_u64(val: u64, out: &mut Write, e: Endian) -> io::Result<()> {
    let mut buf = [0; 8];
    NativeEndian::write_u64(&mut buf, convert_u64(val, e));
    out.write(&buf)?;
    Ok(())
}


/// Converts an u32 of one endian to an f32
pub fn convert_u32_to_float(val: u32, e: Endian) -> f32 {
    let mut buf = [0; 4];
    match e {
        Endian::LITTLE => LittleEndian::write_u32(&mut buf, val),
        Endian::BIG => BigEndian::write_u32(&mut buf, val)
    };
    NativeEndian::read_f32(&buf)
}

/// Reads an f32 from one endian from a Read
pub fn read_f32(inp: &mut Read, e: Endian) -> io::Result<f32> {
    Ok(convert_u32_to_float(read_u32(inp, e)?, host_endian()))
}

/// Converts an u64 of one endian to an f64
pub fn convert_u64_to_float(val: u64, e: Endian) -> f64 {
    let mut buf = [0; 8];
    match e {
        Endian::LITTLE => LittleEndian::write_u64(&mut buf, val),
        Endian::BIG => BigEndian::write_u64(&mut buf, val)
    };
    NativeEndian::read_f64(&buf)
}

/// Reads an f64 from one endian from a Read
pub fn read_f64(inp: &mut Read, e: Endian) -> io::Result<f64> {
    Ok(convert_u64_to_float(read_u64(inp, e)?, host_endian()))
}


/// Converts a f32 to an u32 of a specific endian
pub fn convert_float_to_u32(val: f32, e: Endian) -> u32 {
    let mut buf = [0; 4];
    NativeEndian::write_f32(&mut buf, val);
    match e {
        Endian::LITTLE => LittleEndian::read_u32(&buf),
        Endian::BIG => BigEndian::read_u32(&buf)
    }
}

/// Writes an f32 as one endian to a Write
pub fn write_f32(val: f32, out: &mut Write, e: Endian) -> io::Result<()> {
    write_u32(convert_float_to_u32(val, e), out, host_endian())?;
    Ok(())
}

/// Converts a f64 to an u64 of a specific endian
pub fn convert_float_to_u64(val: f64, e: Endian) -> u64 {
    let mut buf = [0; 8];
    NativeEndian::write_f64(&mut buf, val);
    match e {
        Endian::LITTLE => LittleEndian::read_u64(&buf),
        Endian::BIG => BigEndian::read_u64(&buf)
    }
}

/// Writes an f64 as one endian to a Write
pub fn write_f64(val: f64, out: &mut Write, e: Endian) -> io::Result<()> {
    write_u64(convert_float_to_u64(val, e), out, host_endian())?;
    Ok(())
}

