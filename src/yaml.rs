//! This module contains functions for loading YAML files that are found in return to dream land
//! engine games starting with Planet Robobot. The types found inside these YAML files are
//! compatible to JSON, meaning that converting them from and to JSON is done using serde_json
extern crate simple_error;
extern crate serde_json;

use std::io::{Read, Write, Seek, SeekFrom, Cursor};
use std::str;
use endian;
use self::simple_error::SimpleError;
use xbin;
use self::serde_json::{Value, Number};
use self::serde_json::map::Map;

fn read_tag<R: Read + Seek>(inp: &mut R, e: endian::Endian) -> Result<Value, Box<::std::error::Error>> {
    Ok(match endian::read_u32(inp, e)? {
        1 => Value::Number(Number::from(endian::read_u32(inp, e)?)),
        2 => Value::Number(Number::from_f64(endian::read_f32(inp, e)? as f64).ok_or("Conversion failed")?),
        3 => Value::Bool(endian::read_u32(inp, e)? != 0),
        4 => {
            let pos = endian::read_u32(inp, e)? as u64;
            inp.seek(SeekFrom::Start(pos))?;
            let size = endian::read_u32(inp, e)? as usize;
            let mut vec: Vec<u8> = Vec::with_capacity(size);
            for _ in 0..size {
                vec.push(0);
            }
            inp.read(&mut vec)?;
            let string = str::from_utf8(&vec)?.to_owned();
            Value::String(string.clone())
        },
        5 => {
            let size = endian::read_u32(inp, e)? as usize;
            let mut offs: Vec<(u64, u64)> = Vec::with_capacity(size);
            let mut data: Map<String, Value> = Map::new();
            for _ in 0..size {
                offs.push((endian::read_u32(inp, e)? as u64, endian::read_u32(inp, e)? as u64));
            }
            for (koff, voff) in offs.iter() {
                inp.seek(SeekFrom::Start(*koff))?;
                let ksize = endian::read_u32(inp, e)? as usize;
                let mut vec: Vec<u8> = Vec::with_capacity(size);
                for _ in 0..ksize {
                    vec.push(0);
                }
                inp.read(&mut vec)?;
                let mut key = str::from_utf8(&vec)?.to_owned().clone();
                inp.seek(SeekFrom::Start(*voff))?;
                let mut val = read_tag(inp, e)?;
                data.insert(key, val);
            }
            Value::Object(data)
        },
        6 => {
            let size = endian::read_u32(inp, e)? as usize;
            let mut offs: Vec<u64> = Vec::with_capacity(size);
            let mut vals: Vec<Value> = Vec::with_capacity(size);
            for _ in 0..size {
                offs.push(endian::read_u32(inp, e)? as u64);
            }
            for off in offs.iter() {
                inp.seek(SeekFrom::Start(*off))?;
                let mut tag = read_tag(inp, e)?;
                vals.push(tag);
            }
            Value::Array(vals)
        }
        _ => {Err(SimpleError::new("Unknown tag type"))?}
    })
}

/// This function reads a YAML file from a seekable Read
pub fn load_yaml<R: Read + Seek>(inp: &mut R) -> Result<Value, Box<::std::error::Error>> {
    let xbin = xbin::read_xbin(inp)?;
    let endian = xbin::get_endian(xbin);
    let mut magic = [0;4];
    inp.read(&mut magic)?;
    if &magic != b"YAML" {
        Err(SimpleError::new("Invalid magic"))?;
    }
    inp.read(&mut magic)?;
    read_tag(inp, endian)
}

pub fn get_tag(data: &Value, pos: u32, e: endian::Endian) -> Vec<u8> {
    let mut dat: Vec<u8> = Vec::new();
    {
        let mut of = Cursor::new(&mut dat);
        let out = &mut of;
        match data {
            Value::Number(n) => {
                if n.is_f64() || n.as_u64().unwrap_or(0xFFFFFFFFFFFFFFFF) > 0xFFFFFFFF {
                    endian::write_u32(2, out, e).unwrap();
                    if n.is_f64() {
                        endian::write_f32(n.as_f64().unwrap() as f32, out, e).unwrap();
                    } else {
                        endian::write_f32(n.as_u64().unwrap() as f32, out, e).unwrap();
                    }
                } else {
                    endian::write_u32(1, out, e).unwrap();
                    endian::write_u32(n.as_u64().unwrap() as u32, out, e).unwrap();
                }
            },
            Value::Bool(b) => {
                endian::write_u32(3, out, e).unwrap();
                endian::write_u32(match *b {
                    true => 1,
                    false => 0
                }, out, e).unwrap();
            },
            Value::String(s) => {
                endian::write_u32(4, out, e).unwrap();
                endian::write_u32(pos + 8, out, e).unwrap();
                endian::write_u32(s.len() as u32, out, e).unwrap();
                out.write(s.as_bytes()).unwrap();
                match s.len() % 4 {
                    0 => {},
                    1 => {out.write(&[0;3]).unwrap();},
                    2 => {out.write(&[0;2]).unwrap();},
                    3 => {out.write(&[0;1]).unwrap();},
                    _ => {}
                };
            },
            Value::Object(m) => {
                endian::write_u32(5, out, e).unwrap();
                endian::write_u32(m.len() as u32, out, e).unwrap();
                let mut tmpdata: Vec<u8> = Vec::new();
                {
                    let mut of = Cursor::new(&mut tmpdata);
                    let tmp = &mut of;
                    let mut off = pos + 8 + (m.len() as u32) * 8;
                    for (k, v) in m.iter() {
                        endian::write_u32(off, out, e).unwrap();
                        endian::write_u32(k.len() as u32, tmp, e).unwrap();
                        tmp.write(k.as_bytes()).unwrap();
                        off += k.len() as u32 + 4;
                        match k.len() % 4 {
                            0 => {},
                            1 => {tmp.write(&[0;3]).unwrap(); off += 3;},
                            2 => {tmp.write(&[0;2]).unwrap(); off += 2;},
                            3 => {tmp.write(&[0;1]).unwrap(); off += 1;},
                            _ => {}
                        };

                        endian::write_u32(off, out, e).unwrap();
                        let child = get_tag(v, off, e);
                        tmp.write(&child).unwrap();
                        off += child.len() as u32;
                    }
                }
                out.write(&tmpdata).unwrap();
            },
            Value::Array(l) => {
                endian::write_u32(6, out, e).unwrap();
                endian::write_u32(l.len() as u32, out, e).unwrap();
                let mut tmpdata: Vec<u8> = Vec::new();
                {
                    let mut of = Cursor::new(&mut tmpdata);
                    let tmp = &mut of;
                    let mut off = pos + 8 + (l.len() as u32) * 4;
                    for i in l.iter() {
                        endian::write_u32(off, out, e).unwrap();
                        let child = get_tag(i, off, e);
                        tmp.write(&child).unwrap();
                        off += child.len() as u32;
                    }
                }
                out.write(&tmpdata).unwrap();
            },
            Value::Null => {
                endian::write_u32(3, out, e).unwrap();
                endian::write_u32(0, out, e).unwrap();
            }
        };
    }
    dat
}

/// This function saves a YAML file to a seekable Write
pub fn save_yaml(xbin: xbin::Version, data: &Value, out: &mut Write) -> Result<(), Box<::std::error::Error>> {
    let off = xbin::get_start_off(xbin);
    let endian = xbin::get_endian(xbin);
    let output = get_tag(data, off as u32, endian);
    xbin::write_xbin(xbin, out, off + output.len())?;
    out.write(&output)?;
    Ok(())
}
