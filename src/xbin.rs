//! This module contains utility functions for creating, editing and reading XBIN files (the main
//! container format used in RTDL engine games)

extern crate simple_error;

use std::io::{Read, Write};
use endian;
use self::simple_error::SimpleError;

/// This enum contains the versions of XBIN that are supported by this crate and their header
#[derive(Copy, Clone)]
pub enum Version {
    /// Version 2 XBIN files. Header size 16 bytes. Used in Return to Dream Land, 20th 
    /// Anniversary Collection, Triple Deluxe, Kirby's Fighters Deluxe, Dedede Drum Dash Deluxe and
    /// Kirby and the Rainbow Curse/Paintbrush
    VER2{
        endian: endian::Endian,
        size: u32,
        kind: u32
    },
    /// Version 4 XBIN files. Header size is 20 bytes. Used in Planet Robobot, Team Kirby Clash
    /// Deluxe, Blowout Blast, Kirby: Battle Royale, Star Allies and potential future games.
    VER4{
        endian: endian::Endian,
        size: u32,
        kind: u32,
        uid: u32
    }
}

/// This function returns the byte offset that the data starts at. This is important as the offsets
/// inside of the files are based on the begin of the XBIN file and not on the start of the data.
pub fn get_start_off(ver: Version) -> usize {
    match ver {
        Version::VER2 { endian: _, size: _, kind: _} => 0x10,
        Version::VER4 { endian: _, size: _, kind: _, uid: _} => 0x14
    }
}

/// Returns the endian of the XBIN file
pub fn get_endian(ver: Version) -> endian::Endian {
    match ver {
        Version::VER2 { endian, size: _, kind: _} => endian,
        Version::VER4 { endian, size: _, kind: _, uid: _} => endian
    }
}

/// This function reads an XBIN header. It returns an error whin
///
/// - The file is not a valid XBIN file
/// - The version of the XBIN file is not supported
/// - The file is truncated
pub fn read_xbin(inp: &mut Read) -> Result<Version, Box<::std::error::Error>> {
    let mut header = [0; 4];
    inp.read(&mut header)?;
    if &header != b"XBIN" {
        Err(SimpleError::new("Invalid magic number"))?;
    }
    let endian = match endian::read_u16(inp, endian::Endian::LITTLE)? {
        0x1234 => endian::Endian::LITTLE,
        0x3412 => endian::Endian::BIG,
        _ => Err(SimpleError::new("Invalid endian value"))?
    };
    let version_num = match endian::read_u16(inp, endian::Endian::LITTLE)? {
        2 => 2,
        4 => 4,
        _ => Err(SimpleError::new("Unknown version"))?
    };
    let size = endian::read_u32(inp, endian)?;
    let kind = endian::read_u32(inp, endian)?;
    if version_num == 2 {
        return Ok(Version::VER2 {
            endian,
            size,
            kind
        });
    } else {
        let uid = endian::read_u32(inp, endian)?;
        return Ok(Version::VER4 {
            endian,
            size,
            kind,
            uid
        });
    }
}

/// This function writes an XBIN header. It returns an error when
///
/// - The file can't be written to
/// - The size of the file is too large (max 4GB)
pub fn write_xbin(ver: Version, out: &mut Write, size: usize) -> Result<(), Box<::std::error::Error>> {
    if size > 0xFFFFFFFF {
        Err(SimpleError::new("File size too large"))?;
    }
    match ver {
        Version::VER2 { endian, size, kind } => {
            out.write(b"XBIN")?;
            endian::write_u16(0x1234, out, endian)?;
            endian::write_u16(2, out, endian::Endian::LITTLE)?;
            endian::write_u32(size as u32, out, endian)?;
            endian::write_u32(kind, out, endian)?;
        },
        Version::VER4 { endian, size, kind, uid } => {
            endian::write_u16(0x1234, out, endian)?;
            endian::write_u16(4, out, endian::Endian::LITTLE)?;
            endian::write_u32(size as u32, out, endian)?;
            endian::write_u32(kind, out, endian)?;
            endian::write_u32(uid, out, endian)?;
        }
    };
    Ok(())
}
